package ru.tsc.marfin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.model.Project;
import ru.tsc.marfin.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-remove-by-index";

    @NotNull
    public static final String DESCRIPTION = "Remove project by index";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        @NotNull final Project project = getProjectService().findOneByIndex(userId, index);
        serviceLocator.getProjectTaskService().removeProjectById(userId, project.getId());
    }

}
